/*
 *   openMMC -- Open Source modular IPM Controller firmware
 *
 *   Copyright (C) 2015  Henrique Silva <henrique.silva@lnls.br>
 *   Copyright (C) 2015  Piotr Miedzik  <P.Miedzik@gsi.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

/* Kernel includes. */
#include "FreeRTOS.h"

/* Project includes */
#include "GitSHA1.h"
#include "port.h"
#include "led.h"
#include "pin_cfg.h"
#include "ipmi.h"
#ifdef MODULE_PAYLOAD
#include "payload.h"
#endif
#include "i2c.h"
#include "fru.h"
#include "scansta1101.h"
#include "fpga_spi.h"
#ifdef MODULE_FPGA_UART
#include "fpga_uart.h"
#endif
#include "watchdog.h"
#include "uart_debug.h"
#ifdef MODULE_RTM
#include "rtm.h"
#endif

/*-----------------------------------------------------------*/
int main( void )
{
    pin_init();

#ifdef MODULE_UART_DEBUG
    uart_init( UART_DEBUG );
#endif

    printf("openMMC Starting!\n");
    printf("Build date: %s %s\n", __DATE__, __TIME__);
    printf("Version: %s\n", g_GIT_TAG);
    printf("SHA1: %s\n", g_GIT_SHA1);

#ifdef BENCH_TEST
    printf("BENCH_TEST mode activated! This will enable some debug functions, be careful!\n");
#endif

#ifdef MODULE_WATCHDOG
    watchdog_init();
#endif

    LED_init();
    i2c_init();

    ipmb_addr = get_ipmb_addr( );

#ifdef MODULE_FRU
    printf("FRU_Init\n");
    fru_init(FRU_AMC);
#endif

#ifdef MODULE_SDR
    printf("SDR_Init\n");
    sdr_init();
#endif
    
#ifdef MODULE_SENSORS
    printf("SENSOR_Init\n");
    sensor_init();
#endif

#ifdef MODULE_PAYLOAD
    printf("PAYLOAD_Init\n");
    payload_init();
#endif

#ifdef MODULE_SCANSTA1101
    printf("SCANSTA_Init\n");
    scansta1101_init();

#endif
#ifdef MODULE_FPGA_SPI
    printf("FPGA_SPI_Init\n");
    fpga_spi_init();
#endif
#ifdef MODULE_FPGA_UART
    printf("FPGA_UART_Init\n");
    fpga_uart_init();
#endif

#ifdef MODULE_RTM
    printf("RTM_Manage_Init\n");
    rtm_manage_init();
#endif
    /*  Init IPMI interface */
    /* NOTE: ipmb_init() is called inside this function */

    printf("IPMI_Init\n");
    ipmi_init();

    SystemCoreClockUpdate();
    uint32_t  tmp = RCC->CFGR & RCC_CFGR_SWS;
    printf("SWS %x coreclock %d\n", tmp, SystemCoreClock );

    /* Start the tasks running. */
    printf("StartTaskScheduler\n");
    vTaskStartScheduler();

    /* If all is well we will never reach here as the scheduler will now be
       running.  If we do reach here then it is likely that there was insufficient
       heap available for the idle task to be created. */
    for( ;; );

}


/* FreeRTOS debug functions */
#if (configUSE_MALLOC_FAILED_HOOK == 1)
void vApplicationMallocFailedHook( void ) {}
#endif

#if (configCHECK_FOR_STACK_OVERFLOW == 1)
void vApplicationStackOverflowHook ( TaskHandle_t pxTask, signed char * pcTaskName)
{
    (void) pxTask;
    (void) pcTaskName;
    taskDISABLE_INTERRUPTS();
    /* Place a breakpoint here, so we know when there's a stack overflow */
    for ( ; ; ) {
	UBaseType_t ret;
        ret = uxTaskGetStackHighWaterMark(pxTask);
	printf("%s: task %s uxTaskGetStackHighWaterMark = %d\n", __func__, pcTaskName, (int) ret);
//	configASSERT(0);
    }
}
#endif

