/*
 *   openMMC -- Open Source modular IPM Controller firmware
 *
 *   Copyright (C) 2019 CERN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */
/**
 * @file fpga_uart.c
 * @author Adam Wujek  <adam.wujek@cern.ch>, CERN
 *
 * @brief Interface functions for FPGA via uart
 *
 * @ingroup FPGA_UART
 */

#include "FreeRTOS.h"
#include "task.h"
#include "port.h"

#define IUART_PLATFORM_HOSTED

#include "iuart.h"
#include "fpga_uart.h"
#include "task_priorities.h"
#include "sdr.h"
#include "ipmi.h"


#define LONG_TIME 0xffff
#define OVERRUN_UART  1
#define OVERRUN_RBUFF 2

#define UART_BUFFER_SIZE 2048

#define ERTM14_IUART_MSG_MMC_UPDATE 0 
#define ERTM14_IUART_MSG_IPMI_CONSOLE_REQ 2
#define ERTM14_IUART_MSG_IPMI_SNMP_REQ 3
#define ERTM14_IUART_MSG_IPMI_CONSOLE_RESP 4
#define ERTM14_IUART_MSG_IPMI_SNMP_RESP 5
#define ERTM14_IUART_MSG_IPMI_REBOOT_FPGA 6

#define ERTM14_MSG_IPMI_COMPLETION 7
#define ERTM14_MSG_IPMI_MORE_PAYLOAD_FLAG 0x80

struct uart_buffer
{
    uint8_t data[ UART_BUFFER_SIZE ];
    int head, tail, count, size;
    SemaphoreHandle_t lock;
};

static struct iuart_device dev_iuart;
SemaphoreHandle_t iuart_mutex;


extern void fpga_soft_reset( void );

static void ubuf_put( struct uart_buffer* buf, int ch, int is_isr, BaseType_t *higherTaskPrioWoken )
{
//    printf("put %x count %d size %d\n", ch, buf->count, buf->size );
    if (buf->count >= buf->size)
		return;

    /*if( is_isr )
        xSemaphoreTakeFromISR( buf->lock, higherTaskPrioWoken );
    else
        xSemaphoreTake( buf->lock, LONG_TIME );*/

    if(!is_isr) 
        portENTER_CRITICAL();

    buf->data[buf->head] = ch;
	buf->head++;
    buf->count++;

	if (buf->head >= buf->size)
		buf->head = 0;

    if(!is_isr) 
        portEXIT_CRITICAL();

    /*if( is_isr )
        xSemaphoreGiveFromISR( buf->lock, higherTaskPrioWoken );
    else
        xSemaphoreGive( buf->lock );*/

}

static int ubuf_get( struct uart_buffer* buf, int is_isr, BaseType_t *higherTaskPrioWoken )
{
    if( !buf->count )
        return -1;

/*    if( is_isr )
        xSemaphoreTakeFromISR( buf->lock, higherTaskPrioWoken );
    else
        xSemaphoreTake( buf->lock, LONG_TIME );*/

    if(!is_isr) 
        portENTER_CRITICAL();

	int rv = buf->data[buf->tail];

    buf->tail++;
    if (buf->tail >= buf->size)
		buf->tail = 0;
    buf->count--;

    if(!is_isr) 
        portEXIT_CRITICAL();

/*    if( is_isr )
        xSemaphoreGiveFromISR( buf->lock, higherTaskPrioWoken );
    else
        xSemaphoreGive( buf->lock );*/

    return rv;
}


static inline int ubuf_empty( struct uart_buffer* buf )
{
    return ( buf->count == 0 );
}

static inline int ubuf_init( struct uart_buffer *buf, int size )
{
    buf->head = buf->tail = buf->count = 0;
    buf->size = size;

    buf->lock = xSemaphoreCreateMutex();
    return 0;
}

static struct uart_buffer uart3_rx_buf, uart3_tx_buf;

#if 0
uint16_t recv_char = 'v';
#define RX_RBUFF_SIZE	33
static uint8_t rx_rbuff[RX_RBUFF_SIZE];
static uint8_t rx_rbuff_head = 0;
static uint8_t rx_rbuff_tail = 0;
static uint8_t uart3_overrun = 0;
static volatile uint8_t *uart3_buff_curr;
static volatile uint8_t *uart3_buff_end;
SemaphoreHandle_t uart3_tx_mutex;
SemaphoreHandle_t uart3_rx_mutex;

uint8_t uart_irq_recv(int id);

void rx_rbuff_inc_p(uint8_t *old)
{
    (*old)++;
    *old = *old % RX_RBUFF_SIZE;
}

void rx_buff_put(uint8_t item) {
    rx_rbuff[rx_rbuff_head] = item;
    rx_rbuff_inc_p(&rx_rbuff_head);
    if (rx_rbuff_head == rx_rbuff_tail) {
	uart3_overrun |= OVERRUN_RBUFF;
	rx_rbuff_inc_p(&rx_rbuff_tail);
    }
}

uint8_t rx_buff_get(void) {
    uint8_t item;
    while (rx_rbuff_head == rx_rbuff_tail) {
	/* buffer empty, wait for something */
	while( xSemaphoreTake( uart3_rx_mutex, LONG_TIME ) != pdTRUE ) {
	    /* wait for RX to complete */
	}
    }

    item = rx_rbuff[rx_rbuff_tail];
    rx_rbuff_inc_p(&rx_rbuff_tail);
    return item;
}
#endif

volatile int txcnt =0 ;
volatile int rxcnt =0 ;

void USART3_IRQHandler(void)
{
    BaseType_t xHigherPriorityTaskWoken = 0;

    // handle TX interrupt
    if (USART_GetITStatus(USART3, USART_IT_TXE) == SET)
    {
        if( ubuf_empty( &uart3_tx_buf) )
        {
       	    USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
        }
        else
        {
            uint8_t c = (uint8_t) ubuf_get( &uart3_tx_buf, 1, &xHigherPriorityTaskWoken );
	        USART_SendData(usart_cfg[3].dev, c );
            txcnt++;
        }
    }

    if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
    {
        volatile uint8_t ch = USART_ReceiveData(usart_cfg[3].dev);
        //printf("Rx %x\n\r", ch );
        ubuf_put( &uart3_rx_buf, ch, 1, &xHigherPriorityTaskWoken );
        rxcnt++;
    }

    if (USART_GetITStatus(USART3, USART_IT_ORE) == SET) 
    {
    	//("RX overrrun!!!!!!!!\n");
    	USART_ClearFlag(USART3, USART_FLAG_ORE);
    }

    /* If xHigherPriorityTaskWoken was set to true you
    we should yield.  The actual macro used here is
    port specific. */
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

/* non-blocking send using irqs */
void uart_irq_send(uint8_t *buff, int len)
{
    int i;
    for( i = 0; i < len; i++ )
        ubuf_put( &uart3_tx_buf, buff[i], 0, NULL );
    USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
}



/* non-blocking send using irqs */
int uart_irq_recv()
{
    //printf("rxsize %d\n", uart3_rx_buf.count );
    return ubuf_get( &uart3_rx_buf, 0, NULL );
}


//   USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

void USART3_IRQConfig(void) 
{
  NVIC_InitTypeDef NVIC_InitStructure;
  /* NVIC configuration */
  /* Configure the Priority Group to 4 bits */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0xf;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

static void iuart_send_byte_hook(struct iuart_device* dev, uint8_t b)
{
    uart_irq_send( &b, 1 );
}

static int iuart_recv_byte_hook(struct iuart_device* dev)
{
    return uart_irq_recv();
}

/* Send board data to the FPGA RAM via SPI periodically */
void vTaskFPGA_COMM( void * Parameters )
{
    ubuf_init( &uart3_rx_buf, UART_BUFFER_SIZE );
    ubuf_init( &uart3_tx_buf, UART_BUFFER_SIZE );
    uart_init(FPGA_UART);
    USART3_IRQConfig();
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);

    iuart_init_hosted(&dev_iuart);
    dev_iuart.send_byte = iuart_send_byte_hook;
    dev_iuart.recv_byte = iuart_recv_byte_hook;
    iuart_mutex = xSemaphoreCreateMutex();

    const TickType_t xFrequency = FPGA_UPDATE_RATE;
    TickType_t xLastWakeTime = xTaskGetTickCount();

    vTaskDelay(2000);

    printf("Starting Fpga_Comm task\n");

    /* int for uart? */
    for ( ;; )
    {
        vTaskDelay(10);
    }
}


void fpga_uart_init( void )
{
    xTaskCreate(vTaskFPGA_COMM,      "FPGA_COMM",      150, NULL, tskFPGA_COMM_PRIORITY, (TaskHandle_t *) NULL);
    //xTaskCreate(vTaskFPGA_COMM_read, "FPGA_COMM_read", 150, NULL, tskFPGA_COMM_PRIORITY, (TaskHandle_t *) NULL);
}

#define MAX_PAYLOAD_SIZE 128

#define IPMI_MAX_REQUEST_PAYLOAD (105)
#define IPMI_MAX_RESPONSE_PAYLOAD (13)

static uint8_t ipmi_completion_buf[MAX_PAYLOAD_SIZE];
static int ipmi_completion_size;

static int min(int x, int y )
{
    return x<y?x:y;
}

/* IPMI Handlers */
IPMI_HANDLER(ipmi_custom_snmp, NETFN_CUSTOM, 0, ipmi_msg * req, ipmi_msg * rsp )
{
    int timeout = 1000; // ms

    if( req->data_len < 1 )
    {
        rsp->completion_code = IPMI_CC_REQ_DATA_NOT_PRESENT;
        rsp->data_len = 0;
        return ;
    }

    uint8_t type = req->data[0];

    switch(type)
    {
        case ERTM14_IUART_MSG_IPMI_REBOOT_FPGA:
            printf("Triggering FPGA reboot...\n\r");
            fpga_soft_reset();
            rsp->completion_code = IPMI_CC_OK;
            rsp->data_len = 0;
            return;
        case ERTM14_MSG_IPMI_COMPLETION: // host requesting completion of last IPMI response
        {
            int offset = req->data[1];
            // fixme: validate offset/size

            int to_copy = min( ipmi_completion_size - offset, IPMI_MAX_RESPONSE_PAYLOAD );
            int remaining = ipmi_completion_size - offset - to_copy;

            /*printf("completion: %d %d %d ", offset, to_copy, remaining );
            int i;
            for(i = 0; i < to_copy; i++)
                printf("%02x ", ipmi_completion_buf[offset+i]);
            printf("\n\r");*/

            rsp->data[0] = ERTM14_MSG_IPMI_COMPLETION;
            if( remaining > 0 )
            {
                rsp->data[0] |= ERTM14_MSG_IPMI_MORE_PAYLOAD_FLAG;
            }
            memcpy(rsp->data + 1, ipmi_completion_buf + offset, to_copy);
            
            rsp->completion_code = IPMI_CC_OK;
            rsp->data_len = 1 + to_copy;
            return;
        }
    }

    xSemaphoreTake( iuart_mutex, LONG_TIME );

    printf("fwd %x %d\n\r", req->data[0], req->data_len );

    // just forward to the FPGA...
    iuart_send_message( &dev_iuart, req->data, req->data_len );
    do
    {
        int rv = iuart_recv_message( &dev_iuart );

        if (rv == START_INSN_CHAR_VAL) 
            break;

        vTaskDelay(1);
    } while ( timeout-- );

    xSemaphoreGive( iuart_mutex );


    if( timeout > 0 )
    {
        int rsp_len = dev_iuart.rx_csize;

        if( rsp_len > MAX_PAYLOAD_SIZE )
        {
            rsp->completion_code = IPMI_CC_REQ_DATA_FIELD_EXCEED;
            rsp->data_len = 0;
            return;
        }

        
        int i;
        /*printf("IUART dmp:");
        for(i=0;i<rsp_len;i++)
            printf(" %02x", dev_iuart.rx_buf[i]);
        printf("\n\r");*/

        memcpy(ipmi_completion_buf, dev_iuart.rx_buf, rsp_len);
        ipmi_completion_size = rsp_len;

        int to_send = min( IPMI_MAX_RESPONSE_PAYLOAD, rsp_len );

        rsp->data[0] = ipmi_completion_buf[0];
        if( to_send < rsp_len )
            rsp->data[0] |= ERTM14_MSG_IPMI_MORE_PAYLOAD_FLAG;

        //printf("IUartRp type %02x length %d to_send %d \n\r", rsp->data[0], rsp_len, to_send );

        memcpy(rsp->data + 1, dev_iuart.rx_buf + 1, to_send - 1);
        rsp->completion_code = IPMI_CC_OK;
        rsp->data_len = to_send;
    }
    else
    {
        printf("IUart timeout [%d %d]\n\r", txcnt, rxcnt );

        rsp->completion_code = IPMI_CC_TIMEOUT;
        rsp->data_len = 0;
    }
}
